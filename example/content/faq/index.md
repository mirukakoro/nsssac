---
title: "FAQ"
date: 2020-12-20T11:37:07-05:00
draft: false
---

# FAQ

On this page you will find frequently asked questions about the SAC and Northern.

## How do I Reset My TDSB Student Account Password?

You can reset your password [here](https://pims.tdsb.on.ca/Home.aspx/ForgotPassword).

## Where can I Find Other Info About Northern?

You can find a lot of info including floor plans, calendars, and more at the 
[NSS Student Agenda](https://sites.google.com/tdsb.on.ca/nssstudentagenda2020-2021/home), and the 
[NSS Website](https://schoolweb.tdsb.on.ca/northernss). Also, you can find info about the TDSB in general at the 
[TDSB website](https://tdsb.on.ca).

## How do I create a new club?

You will only need to fill out 
[this form](https://docs.google.com/forms/d/e/1FAIpQLSfHBsNRxm12PyakaFVQ1JiLV3Pyr95t3MmxdmsUbl8ayhrEIA/viewform), and 
the SAC will handle it from there:

## How do I request money for my club?

You will be able to request money through the 
[Budget Request Form](https://drive.google.com/file/d/1koMzxjnNaoFPIGMjfboOHkkEcu0m4loh/view?usp=sharing), then sending 
it to the SAC Treasurer Gabe Osmond.

## How much money does SAC have?

Our balance is constantly changing as the year goes on, so there is no specific number to define our account, but we 
will always aim to have enough to support any activities that the school needs.

## What will the SAC use its money for?

We will use our monies to run school wide events such as spirit week and guest speakers, as well as fund club and 
association events that will benefit the school.

## Where does the SAC get its money from without the student activity fee?

We are currently using the remaining funds from the 2020 Spring budget that we have redistributed to the SAC general 
account.

## What is Northern’s school song?

Sadly we don’t have an audio clip now, but here’s the lyrics:
```
Hail Dear Old Northern
School of Fame and Learning
Thy praise we’re singing
Hear our voices ringing
Through the years to follow
Always we’ll be true to
Thy colours flying
Red, Gold and Blue
Rah Rah Rah Rah Rah Rah Rah
```
[Source](http://dondrapershair.blogspot.com/2009/01/northerns-school-song.html)
