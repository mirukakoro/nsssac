---
title: "Updates to the SAC Website"
date: 2020-12-20T11:33:16-05:00
draft: false
---

We added some updates to the website, including updating the [Clubs and Associations page]({{< ref "clubs" >}}) 
with new Associations and adding [a FAQ page]({{< ref "faq" >}}) for common questions and answers with links to other 
helpful websites like the [Northern Secondary School website](https://schoolweb.tdsb.on.ca/northernss/). Also, we changed the layout to make it easier to find the info you want faster.
